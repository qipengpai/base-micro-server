##oauth认证的四种模式
* 授权码模式（authorization code）
* 简化模式（implicit）
* 密码模式（resource owner password credentials）
* 客户端模式（client credentials）
![Image text](https://box.kancloud.cn/fdddafa008357b9cb227daa0d50e577f_862x509.png)

http://terasolunaorg.github.io/guideline/5.3.0.RELEASE/en/Security/OAuth.html


##spring security oauth2 令牌生成流程：
```
/oauth/token/请求  --> TokenEndpoint -->  （接口）ClientDetailsService(InMemoryClientDetailsService)
 --> ClientDetails --> TokenRequest --> （接口）TokenGraner(CompositeTokenGranter) 
 --> Oauth2Request + （接口）Authentication -->Oauth2Authentication 
 --> （接口） AuthorizationServerTokenServices(DefaultTokenServices)-->OAuth2AccessToken 
```
##说明：

* TokenEndpoint:  简单来说 TokenEndpoint 如同Controller 一样 监听/oauth/token/ 

* ClientDetailsService(InMemoryClientDetailsService):  这个service就是获取当前请求的第三方，clientId 和 secret

* ClientDetails:  由TokenEndpoint创建 放入第三方信息

* TokenRequest : 由TokenEndpoint创建 放入其他请求信息

* TokenGraner(CompositeTokenGranter) ： 这里面封装了4种授权模式，看请求的参数使用哪种授权模式

* Oauth2Request: 这TokenGraner创建 是ClientDetails 和 TokenRequest  的信息整合

* Authentication ： 这里就是封装好授权的用户（由Security的UserDetailService 获取的）

* Oauth2Authentication ： 由 Oauth2Request和 Authentication 组合成的，把之前的信息封装好：是哪个第三方，然后是哪个用户授权，用的是哪个授权模式，授权的参数是什么

* AuthorizationServerTokenServices(DefaultTokenServices)： 根据Oauth2Authentication生成令牌   方法：TokenStroe（设置令牌的存取） TokenEnhancer(令牌的增强器，往令牌加东西)
