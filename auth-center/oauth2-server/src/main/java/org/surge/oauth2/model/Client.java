package org.surge.oauth2.model;

import lombok.Data;

import java.io.Serializable;

/**
 * Oauth2.0客户端
 * @author qipengai
 * @date 2019/6/20 20:53
 * @since 0.0.1
 */
@Data
public class Client implements Serializable{
   private static final long serialVersionUID = -2838988322596336035L;

   private Long id;
   private String clientId;
   private String resourceIds = "";
   private String clientSecret;
   private String clientSecretStr;
   private String scope = "all";
   private String authorizedGrantTypes = "authorization_code,password,refresh_token,client_credentials";
   private String webServerRedirectUri;
   private String authorities = "";
   private Integer accessTokenValidity = 18000;
   private Integer refreshTokenValidity = 18000;
   private String additionalInformation = "{}";
   private String autoapprove = "true";
   
}
