package org.surge.oauth2.controller;

import org.surge.common.result.PageResult;
import org.surge.common.result.Result;
import org.surge.oauth2.dto.ClientDto;
import org.surge.oauth2.model.SysService;
import org.surge.oauth2.service.SysServiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 服务相关接口
 * @author: qipengpai
 * @since: 0.0.1
 */
@RestController
@Api(tags = "服务模块api")
@RequestMapping("/services")
public class SysServiceController {

    @Autowired
    private SysServiceService sysServiceService;

    /**
     * 查询所有服务
     * @return
     */
    @PreAuthorize("hasAuthority('service:get/service/findAlls')")
    @ApiOperation(value = "查询所有服务")
    @GetMapping("/findAlls")
    public PageResult<SysService> findAlls() {
        List<SysService> list = sysServiceService.findAll();
        return new PageResult(list);
    }

    /**
     * 获取服务以及顶级服务
     * @return
     */
    @ApiOperation(value = "获取服务以及顶级服务")
    @GetMapping("/findOnes")
    @PreAuthorize("hasAuthority('service:get/service/findOnes')")
    public PageResult<SysService> findOnes(){
        List<SysService> list = sysServiceService.findOnes();
        return new PageResult(list);
    }

    /**
     * 删除服务
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('service:delete/service/{id}')")
    @ApiOperation(value = "删除服务")
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long id){
        try {
            sysServiceService.delete(id);

        }catch (Exception ex){
            ex.printStackTrace();
            return Result.failed("操作失败");
        }
        return Result.succeed("操作成功");
    }

    @PreAuthorize("hasAnyAuthority('service:post/saveOrUpdate')")
    @ApiOperation(value = "新增服务")
    @PostMapping("/saveOrUpdate")
    public Result saveOrUpdate(@RequestBody SysService service) {
        try{
            if (service.getId() != null){
                sysServiceService.update(service);
            }else {
                sysServiceService.save(service);
            }
            return Result.succeed("操作成功");
        }catch (Exception ex){
            ex.printStackTrace();
            return Result.failed("操作失败");
        }
    }

    @ApiOperation(value = "根据clientId获取对应的服务")
    @GetMapping("/{clientId}/services")
    public List<Map<String, Object>> findServicesByclientId(@PathVariable Long clientId) {
        Set<Long> clientIds = new HashSet<Long>();
        
        //初始化应用
        clientIds.add(clientId);
        
        List<SysService> clientService = sysServiceService.findByClient(clientIds);
        List<SysService> allService = sysServiceService.findAll();
        List<Map<String, Object>> authTrees = new ArrayList<>();

        Map<Long, SysService> clientServiceMap = clientService.stream().collect(Collectors.toMap(SysService::getId, SysService->SysService));

        for (SysService sysService: allService) {
            Map<String, Object> authTree = new HashMap<>();
            authTree.put("id",sysService.getId());
            authTree.put("name",sysService.getName());
            authTree.put("pId",sysService.getParentId());
            authTree.put("open",true);
            authTree.put("checked", false);
            if (clientServiceMap.get(sysService.getId())!=null){
                authTree.put("checked", true);
            }
            authTrees.add(authTree);
        }

        return  authTrees;
    }

    @PostMapping("/granted")
    public Result setMenuToClient(@RequestBody ClientDto clientDto) {
        sysServiceService.setMenuToClient(clientDto.getId(), clientDto.getServiceIds());

        return Result.succeed("操作成功");
    }















}
