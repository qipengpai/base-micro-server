package org.surge.oauth2.service.impl;

import org.surge.common.model.LoginAppUser;
import org.surge.oauth2.feign.UserClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * spring security 自定义用户验证逻辑，取sys_user表
 * @author qipengpai
 * @date 2019/6/20 20:53
 * @since 0.0.1
 */
@Slf4j
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    /**
     * 用户服务 feignClient
     */
    private final UserClient userClient;

    @Autowired
    public UserDetailServiceImpl(UserClient userClient) {
        this.userClient = userClient;
    }

    /**
     * 自定义用户验证逻辑
     * @author qipengpai
     * @date 2019/6/06 12:00
     * @param username 用户名
     * @return org.springframework.security.core.userdetails.UserDetails
     **/
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // feign调用 对外feign restTemplate
        LoginAppUser loginAppUser = userClient.findByUsername(username);
        if (loginAppUser == null) {
            throw new AuthenticationCredentialsNotFoundException("用户不存在");
        } else if (!loginAppUser.isEnabled()) {
            throw new DisabledException("用户已作废");
        }
        return loginAppUser;
    }

}
