package org.surge.oauth2.service;

import org.surge.common.result.PageResult;
import org.surge.common.result.Result;
import org.surge.oauth2.dto.ClientDto;
import org.surge.oauth2.model.Client;

import java.util.List;
import java.util.Map;

public interface ClientService {

	Client getById(Long id) ;
	 
    void saveClient(ClientDto clientDto);

    Result saveOrUpdate(ClientDto clientDto);

    void deleteClient(Long id);
    
    PageResult<Client> listRoles(Map<String, Object> params);
    
    List<Client> findList(Map<String, Object> params) ;
    
    List<Client> listByUserId(Long userId) ;
    
}
