package org.surge.oauth2.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.surge.oauth2.token.RedisTemplateTokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * 存储token
 * @author qipengpai
 * @date 2019/6/25 20:17
 * @since 0.0.1
 */
@Configuration
public class TokenStoreConfig  {

	@Resource
	private DataSource dataSource;

	@Resource
	private RedisTemplate<String, Object>  redisTemplate;


	/**
	 * 数据库存储token
	 * @author qipengpai
	 * @date 2019/6/25 20:17
	 * @return org.springframework.security.oauth2.provider.token.store.JdbcTokenStore
	 */
	@Bean
	@ConditionalOnProperty(prefix="security.oauth2.token.store",name="type" ,havingValue="jdbc" ,matchIfMissing=false)
	public JdbcTokenStore jdbcTokenStore(){
		// oauth_access_token oauth_refresh_token 创建两张表
		return new JdbcTokenStore(dataSource) ;

	}
	/**
	 * 使用redis方式存储token
	 * @author qipengpai
	 * @date 2019/6/4 13:41
	 * @return com.bccv.nmg.server.token.RedisTemplateTokenStore
	 */
	@Bean
	@ConditionalOnProperty(prefix="security.oauth2.token.store",name="type" ,havingValue="redis" ,matchIfMissing=true)
	public RedisTemplateTokenStore redisTokenStore(){
		// return new RedisTokenStore( redisTemplate.getConnectionFactory() ) ; //单台redis服务器
		Assert.state(redisTemplate != null, "RedisTemplate must be provided");

		RedisTemplateTokenStore redisTemplateStore = new RedisTemplateTokenStore()  ;
		redisTemplateStore.setRedisTemplate(redisTemplate);
		return redisTemplateStore ;
	}
	
}
