package org.surge.oauth2.config;

import org.surge.common.props.PermitUrlProperties;
import org.surge.oauth2.service.RedisAuthorizationCodeServices;
import org.surge.oauth2.service.RedisClientDetailsService;
import org.surge.oauth2.token.RedisTemplateTokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.code.RandomValueAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

/**
 * OAuth2.0配置
 * @author qipengpai
 * @date 2019/6/25 20:05
 * @since 0.0.1
 */
@Configuration
public class OAuth2ServerConfig {

    @Resource
    private DataSource dataSource;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 声明 ClientDetails实现(9)
     * @author qipengpai
     * @date 2019/6/25 20:06
     * @return com.bccv.nmg.server.service.RedisClientDetailsService
     **/
    @Bean
    public RedisClientDetailsService redisClientDetailsService() {
        RedisClientDetailsService clientDetailsService = new RedisClientDetailsService(dataSource);
        clientDetailsService.setRedisTemplate(redisTemplate);
        return clientDetailsService;
    }

    /**
     * 重写授权码存取策略(10)
     * @author qipengpai
     * @date 2019/6/25 20:07
     * @return org.springframework.security.oauth2.provider.code.RandomValueAuthorizationCodeServices
     */
    @Bean
    public RandomValueAuthorizationCodeServices authorizationCodeServices() {
        RedisAuthorizationCodeServices redisAuthorizationCodeServices = new RedisAuthorizationCodeServices();
        redisAuthorizationCodeServices.setRedisTemplate(redisTemplate);
        return redisAuthorizationCodeServices;
    }

    /**
     *
     * 默认token存储在内存中
     * DefaultTokenServices 默认处理
     * AuthorizationServer 认证服务器
     * @author qipengpai
     * @date 2019/6/25 20:06
     * @since 0.0.1
     */
    @Component
    @Configuration
    @EnableAuthorizationServer
    @AutoConfigureAfter(AuthorizationServerEndpointsConfigurer.class)
    public static class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
        /**
         * 注入authenticationManager 来支持 password grant type 认证管理重要
         */
        @Autowired
        private AuthenticationManager authenticationManager;

        /**
         * 自定义用户验证逻辑接口实现 loadUserByUsername
         */
        @Autowired
        private UserDetailsService userDetailsService;

        /**
         * redis方式存储token
         */
        @Autowired(required = false)
        private RedisTemplateTokenStore redisTokenStore;

        /**
         * 配置异常翻译
         */
        @Autowired
        private WebResponseExceptionTranslator webResponseExceptionTranslator;

        /**
         * 为装载OAuth2的客户端重写
         **/
        @Autowired
        private RedisClientDetailsService redisClientDetailsService;

        /**
         * 授权码缓存redis里面(重写)
         */
        @Autowired(required = false)
        private RandomValueAuthorizationCodeServices authorizationCodeServices;

        /**
         *  配置身份认证器，配置认证方式，TokenStore，TokenGranter，OAuth2RequestFactory(11)
         * @author qipengpai
         * @date 2019/6/4 16:07
         * @param endpoints 定义授权和令牌端点和令牌服务
         * @return void
         */
        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {

            /*
             * 目前使用redis,通过注入一个AuthenticationManager来打开密码授权
             */
            if (redisTokenStore != null) {
                endpoints.tokenStore(redisTokenStore).authenticationManager(authenticationManager)
                        // 自定义用户验证逻辑
                        .userDetailsService(userDetailsService);
            }

            /*
             * JdbcAuthorizationCodeServices替换
             * 授权码缓存redis里面，RedisAuthorizationCodeServices，解决启动多个认证中心授权码问题
             */
            endpoints.authorizationCodeServices(authorizationCodeServices);

            /*
             * 配置异常翻译
             * 处理 ExceptionTranslationFilter 抛出的异常
             */
            endpoints.exceptionTranslator(webResponseExceptionTranslator);

        }

        /**
         * 配置应用名称 应用id 配置OAuth2的客户端相关信息(12)
         * @author qipengpai
         * @date 2019/6/4 16:07
         * @param clients 定义客户端详细信息服务的配置程序。客户端细节可以被初始化，也可以直接引用现有的存储 redis或JDBC
         **/
        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.withClientDetails(redisClientDetailsService);
            redisClientDetailsService.loadAllClientToCache();
        }


        /**
         * 对应于配置AuthorizationServer安全认证的相关信息，创建ClientCredentialsTokenEndpointFilter核心过滤器(13)
         * @author qipengpai
         * @date 2019/6/25 16:08
         * @param security 定义令牌端点上的安全约束。配置token获取合验证时的策略
         **/
        @Override
        public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
            security.tokenKeyAccess("permitAll()")
                    //已验证的
                    .checkTokenAccess("isAuthenticated()")
                    // allow check token
                    .allowFormAuthenticationForClients();
        }

    }


    /**
     * ResourceServer资源服务器配置
     *
     * <p> 要访问资源服务器受保护的资源需要携带令牌（从授权服务器获得）
     * 客户端往往同时也是一个资源服务器，各个服务之间的通信（访问需要权限的资源）时需携带访问令牌
     * 资源服务器通过 @EnableResourceServer 注解来开启一个 OAuth2AuthenticationProcessingFilter 类型的过滤器
     * 通过继承 ResourceServerConfigurerAdapter 类来配置资源服务器x
     *
     * @author qipengpai
     * @date 2019/5/31 10:58
     */
    @Configuration
    @EnableResourceServer
    @EnableConfigurationProperties(PermitUrlProperties.class)
    public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

        @Autowired
        private PermitUrlProperties permitUrlProperties;

        /**
         * 有token时或者指定接口资源服务器配置(14)
         * @author qipengpai
         * @date 2019/6/25 16:09
         * @param http HttpSecurity
         **/
        @Override
        public void configure(HttpSecurity http) throws Exception {
            // 该方法有很多子方法，每个子匹配器将会按照声明的顺序起作用。
            http.requestMatcher(
                    /**
                     * 判断来源请求是否包含oauth2授权信息
                     */
                    new RequestMatcher() {
                        private AntPathMatcher antPathMatcher = new AntPathMatcher();

                        @Override
                        public boolean matches(HttpServletRequest request) {
                            // 请求参数中包含access_token参数
                            if (request.getParameter(OAuth2AccessToken.ACCESS_TOKEN) != null) {
                                return true;
                            }

                            // 头部的Authorization值以Bearer开头
                            String auth = request.getHeader("Authorization");
                            if (auth != null) {
                                if (auth.startsWith(OAuth2AccessToken.BEARER_TYPE)) {
                                    return true;
                                }
                            }
                            if (antPathMatcher.match(request.getRequestURI(), "/oauth/userinfo")) {
                                return true;
                            }
                            if (antPathMatcher.match(request.getRequestURI(), "/oauth/remove/token")) {
                                return true;
                            }
                            if (antPathMatcher.match(request.getRequestURI(), "/oauth/get/token")) {
                                return true;
                            }
                            if (antPathMatcher.match(request.getRequestURI(), "/oauth/refresh/token")) {
                                return true;
                            }

                            if (antPathMatcher.match(request.getRequestURI(), "/oauth/token/list")) {
                                return true;
                            }

                            if (antPathMatcher.match("/clients/**", request.getRequestURI())) {
                                return true;
                            }

                            if (antPathMatcher.match("/services/**", request.getRequestURI())) {
                                return true;
                            }
                            if (antPathMatcher.match("/redis/**", request.getRequestURI())) {
                                return true;
                            }
                            return false;
                        }
                    }

            ).authorizeRequests()
            // 配置中定义的忽略路径被许可
            .antMatchers(permitUrlProperties.getIgnored()).permitAll()
            .anyRequest().authenticated();// 任何没有匹配上的其他的url请求，只需要用户被验证。
        }

    }

}
