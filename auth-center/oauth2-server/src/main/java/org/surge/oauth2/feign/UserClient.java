package org.surge.oauth2.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.surge.common.model.LoginAppUser;

/**
 * 调用用户中心中的userdetail对象，用户oauth中的登录
 * 获取的用户与页面输入的密码 进行BCryptPasswordEncoder匹配
 * @author qipengpai
 * @date 2019/6/4 13:41
 * @since 0.0.1
 */
@FeignClient("user-server")
public interface UserClient {

	/**
	 * feign rpc访问远程/users-anon/login接口
	 * @author qipengpai
	 * @date 2019/6/4 13:41
	 * @param username 用户名
	 * @return LoginAppUser 登录用户信息
	 */
    @GetMapping(value = "/users-anon/login", params = "username")
	LoginAppUser findByUsername(@RequestParam("username") String username);

    
}
