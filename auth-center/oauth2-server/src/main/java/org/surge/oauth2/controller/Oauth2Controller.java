package org.surge.oauth2.controller;

import org.apache.commons.lang.StringUtils;
import org.surge.common.model.LoginAppUser;
import org.surge.common.result.PageResult;
import org.surge.common.result.Result;
import org.surge.common.util.SpringUtils;
import org.surge.lograbbit.annotation.NmgLog;
import org.surge.oauth2.service.RedisClientDetailsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * OAuth2认证
 * <p>提供认证相关的api.
 * <p>{@link #getUserToken}密码模式（方便理解）.
 * @author qipengpai
 * @since 0.0.1
 */
@Slf4j
@Api(tags = "OAuth2认证")
@RestController
public class Oauth2Controller {

	private final ObjectMapper objectMapper;

	private final PasswordEncoder passwordEncoder;

	private final TokenStore tokenStore;

	private final RedisTemplate<String, Object> redisTemplate;

	/**
	 * 构造器注入
	 * @param objectMapper      Spring启动时自动装配json处理类
	 * @param passwordEncoder   Spring Security中功能强大的加密工具
	 * @param tokenStore        token存储
	 * @param redisTemplate     redis模板
	 */
	@Autowired
	public Oauth2Controller(ObjectMapper objectMapper,PasswordEncoder passwordEncoder, TokenStore tokenStore, RedisTemplate<String, Object> redisTemplate) {
		this.objectMapper = objectMapper;
		this.passwordEncoder = passwordEncoder;
		this.tokenStore = tokenStore;
		this.redisTemplate = redisTemplate;
	}


	/**
	 * 重写了密码模式（方便理解）
	 * @author qipengpai
	 * @date 2019/5/31 10:41
	 * @param username   账号
	 * @param password   密码
	 **/
	@ApiOperation(value = "用户名密码获取token")
	@PostMapping("/oauth/user/token")
	public void getUserToken(
			@ApiParam(required = true, name = "username", value = "账号") @RequestParam(value = "username") String username,
			@ApiParam(required = true, name = "password", value = "密码") @RequestParam(value = "password") String password,
			HttpServletRequest request, HttpServletResponse response) {
		// 客户端ID
		String clientId = request.getHeader("client_id");
		// 客户端密钥
		String clientSecret = request.getHeader("client_secret");
		try {
			// clientId 非空认证
			if (StringUtils.isBlank(clientId)) {
				throw new UnapprovedClientAuthenticationException("请求头中无client_id信息");
			}
			// clientSecret 非空认证
			if (StringUtils.isBlank(clientSecret)) {
				throw new UnapprovedClientAuthenticationException("请求头中无client_secret信息");
			}

			// 获取Bean
			RedisClientDetailsService clientDetailsService = SpringUtils.getBean(RedisClientDetailsService.class);

			// 验证并获取客户端信息
			ClientDetails clientDetails = checkClient(clientDetailsService,clientId,clientSecret);

			TokenRequest tokenRequest = new TokenRequest(new HashMap<>(8), clientId, clientDetails.getScope(),
					"password");

			OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);

			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);

			AuthenticationManager authenticationManager = SpringUtils.getBean(AuthenticationManager.class);

			Authentication authentication = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);

			OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oAuth2Request, authentication);

			AuthorizationServerTokenServices authorizationServerTokenServices = SpringUtils
					.getBean("defaultAuthorizationServerTokenServices", AuthorizationServerTokenServices.class);

			OAuth2AccessToken oAuth2AccessToken = authorizationServerTokenServices
					.createAccessToken(oAuth2Authentication);

			oAuth2Authentication.setAuthenticated(true);

			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(objectMapper.writeValueAsString(oAuth2AccessToken));
			response.getWriter().flush();
			response.getWriter().close();

		} catch (Exception e) {
			// 认证异常响应
			catchExceptionResponse(e,response);
		}
	}

	/**
	 * 认证异常响应
	 * @author qipengpai
	 * @date 2019/11/12 11:57
	 * @param e          异常对象
	 * @param response   响应对象
	 */
	private void catchExceptionResponse(Exception e,HttpServletResponse response) {
		// 异常响应
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.setContentType("application/json;charset=UTF-8");
		Map<String, String> rsp = new HashMap<>();
		rsp.put("respCode", HttpStatus.UNAUTHORIZED.value() + "");
		rsp.put("rspMsg", e.getMessage());

		// 响应
		try {
			response.getWriter().write(objectMapper.writeValueAsString(rsp));
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e1) {
			//  Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * 客户端模式（方便理解）
	 * @author qipengpai
	 * @date 2019/5/31 10:43
	 */
	@ApiOperation(value = "clientId获取token")
	@PostMapping("/oauth/client/token")
	@NmgLog(module = "auth-server", type = NmgLog.LOG_TYPE.SELECT, recordRequestParam = false)
	public void getClientTokenInfo(HttpServletRequest request, HttpServletResponse response) {
		// 客户端ID
		String clientId = request.getHeader("client_id");
		// 客户端密钥
		String clientSecret = request.getHeader("client_secret");
		try {
			// clientId 非空认证
			if (StringUtils.isBlank(clientId)) {
				throw new UnapprovedClientAuthenticationException("请求头中无client_id信息");
			}
			// clientSecret 非空认证
			if (StringUtils.isBlank(clientSecret)) {
				throw new UnapprovedClientAuthenticationException("请求头中无client_secret信息");
			}

			// 获取Bean
			RedisClientDetailsService clientDetailsService = SpringUtils.getBean(RedisClientDetailsService.class);

			// 验证并获取客户端信息
			ClientDetails clientDetails = checkClient(clientDetailsService,clientId,clientSecret);


			Map<String, String> map = new HashMap<>(8);
			map.put("client_secret", clientSecret);
			map.put("client_id", clientId);
			map.put("grant_type", "client_credentials");
			TokenRequest tokenRequest = new TokenRequest(map, clientId, clientDetails.getScope(), "client_credentials");

			OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);

			AuthorizationServerTokenServices authorizationServerTokenServices = SpringUtils
					.getBean("defaultAuthorizationServerTokenServices", AuthorizationServerTokenServices.class);
			OAuth2RequestFactory requestFactory = new DefaultOAuth2RequestFactory(clientDetailsService);
			ClientCredentialsTokenGranter clientCredentialsTokenGranter = new ClientCredentialsTokenGranter(
					authorizationServerTokenServices, clientDetailsService, requestFactory);

			clientCredentialsTokenGranter.setAllowRefresh(true);
			OAuth2AccessToken oAuth2AccessToken = clientCredentialsTokenGranter.grant("client_credentials",
					tokenRequest);

			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(objectMapper.writeValueAsString(oAuth2AccessToken));
			response.getWriter().flush();
			response.getWriter().close();

		} catch (Exception e) {
			// 认证异常响应
			catchExceptionResponse(e,response);
		}
	}
	/**
	 * 验证并获取客户端信息
	 * @author qipengpai
	 * @date 2019/11/12 11:49
	 * @param clientDetailsService  客户端bean
	 * @param clientId              客户端Id
	 * @param clientSecret          客户端密钥
	 * @throws UnapprovedClientAuthenticationException clientId对应的信息不存在、clientSecret不匹配
	 * @return org.springframework.security.oauth2.provider.ClientDetails
	 */
	private ClientDetails checkClient(RedisClientDetailsService clientDetailsService, String clientId, String clientSecret) {
		// 客户端信息
		ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);
		// 如果客户端信息不存在或不匹配抛出异常
		if (clientDetails == null) {
			throw new UnapprovedClientAuthenticationException("clientId对应的信息不存在");
		} else if (!passwordEncoder.matches(clientSecret, clientDetails.getClientSecret())) {
			throw new UnapprovedClientAuthenticationException("clientSecret不匹配");
		}
		return clientDetails;
	}

	/**
	 * 刷新token
	 * @author qipengpai
	 * @date 2019/6/13 19:48
	 * @param access_token 令牌
	 * @param request 请求
	 * @param response 响应
	 **/
	@ApiOperation(value = "access_token刷新token")
	@PostMapping(value = "/oauth/refresh/token", params = "access_token")
	public void refreshTokenInfo(String access_token, HttpServletRequest request, HttpServletResponse response) {

		// 拿到当前用户信息
		try {
			Authentication user = SecurityContextHolder.getContext().getAuthentication();

			if (user != null) {
				if (user instanceof OAuth2Authentication) {
					Authentication athentication = (Authentication) user;
					OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) athentication.getDetails();
				}

			}
			OAuth2AccessToken accessToken = tokenStore.readAccessToken(access_token);
			OAuth2Authentication auth = (OAuth2Authentication) user;
			RedisClientDetailsService clientDetailsService = SpringUtils.getBean(RedisClientDetailsService.class);

			ClientDetails clientDetails = clientDetailsService
					.loadClientByClientId(auth.getOAuth2Request().getClientId());

			AuthorizationServerTokenServices authorizationServerTokenServices = SpringUtils
					.getBean("defaultAuthorizationServerTokenServices", AuthorizationServerTokenServices.class);
			OAuth2RequestFactory requestFactory = new DefaultOAuth2RequestFactory(clientDetailsService);

			RefreshTokenGranter refreshTokenGranter = new RefreshTokenGranter(authorizationServerTokenServices,
					clientDetailsService, requestFactory);

			Map<String, String> map = new HashMap<>();
			map.put("grant_type", "refresh_token");
			map.put("refresh_token", accessToken.getRefreshToken().getValue());
			TokenRequest tokenRequest = new TokenRequest(map, auth.getOAuth2Request().getClientId(),
					auth.getOAuth2Request().getScope(), "refresh_token");

			OAuth2AccessToken oAuth2AccessToken = refreshTokenGranter.grant("refresh_token", tokenRequest);

			tokenStore.removeAccessToken(accessToken);

			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(objectMapper.writeValueAsString(oAuth2AccessToken));
			response.getWriter().flush();
			response.getWriter().close();
		} catch (Exception e) {
			// 认证异常响应
			catchExceptionResponse(e,response);
		}

	}

	/**
	 * 获取token信息
	 * @author qipengpai
	 * @date 2019/6/10 16:27
	 * @param access_token 令牌
	 * @return org.springframework.security.oauth2.common.OAuth2AccessToken
	 */
	@ApiOperation(value = "获取token信息")
	@PostMapping(value = "/oauth/get/token", params = "access_token")
	public OAuth2AccessToken getTokenInfo(String access_token) {

		// 拿到当前用户信息
		Authentication user = SecurityContextHolder.getContext().getAuthentication();

		if (user != null) {
			if (user instanceof OAuth2Authentication) {
				Authentication athentication = (Authentication) user;
				OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) athentication.getDetails();
			}

		}
		OAuth2AccessToken accessToken = tokenStore.readAccessToken(access_token);

		return accessToken;

	}

	/**
	 * 登出移除access_token和refresh_token
	 * @author qipengpai
	 * @date 2019/6/13 16:38
	 * @param access_token 令牌
	 **/
	@ApiOperation(value = "移除token")
	@PostMapping(value = "/oauth/remove/token", params = "access_token")
	public void removeToken(String access_token) {

		// 拿到当前用户信息
		Authentication user = SecurityContextHolder.getContext().getAuthentication();

		if (user != null) {
			if (user instanceof OAuth2Authentication) {
				Authentication athentication = (Authentication) user;
				OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) athentication.getDetails();
			}

		}
		//确认token存在
		OAuth2AccessToken accessToken = tokenStore.readAccessToken(access_token);
		if (accessToken != null) {
			// 移除access_token
			tokenStore.removeAccessToken(accessToken);

			// 移除refresh_token
			if (accessToken.getRefreshToken() != null) {
				tokenStore.removeRefreshToken(accessToken.getRefreshToken());
			}

		}
		log.info("用户登出");
	}


	/**
	 * 当前登陆用户信息
	 * security获取当前登录用户的方法是SecurityContextHolder.getContext().getAuthentication()
	 * 这里的实现类是org.springframework.security.oauth2.provider.OAuth2Authentication
	 * @author qipengpai
	 * @date 2019/6/13 16:38
	 * @return org.surge.common.result.Result<java.lang.Object>
	 */
	@ApiOperation(value = "当前登陆用户信息")
	@RequestMapping(value = { "/oauth/userinfo" }, produces = "application/json") // 获取用户信息。/auth/user
	public Result<Object> getCurrentUserDetail() {

        Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Result<Object> result = Result.succeed(object,"认证详细信息");

        log.debug("认证详细信息:" + object.toString());

		log.info("返回信息:{}", result);

		return result;
	}

	/**
	 * 获取token列表
	 * @author qipengpai
	 * @date 2019/11/12 17:50
	 * @param params   条件
	 * @return org.surge.common.result.PageResult<java.util.HashMap<java.lang.String,java.lang.String>>
	 */
	@ApiOperation(value = "token列表")
	@PostMapping("/oauth/token/list")
	public PageResult<HashMap<String, String>> getUserTokenInfo(@RequestParam Map<String, Object> params)
			throws Exception {
		List<HashMap<String, String>> list = new ArrayList<>();

		Set<String> keys = redisTemplate.keys("access:" + "*") ;
		assert keys != null;
		for (Object key: keys) {

            OAuth2AccessToken token = (OAuth2AccessToken)redisTemplate.opsForValue().get(key);
			HashMap<String, String> map = new HashMap<String, String>();
			assert token != null;
			map.put("token_type", token.getTokenType());
			map.put("token_value", token.getValue());
			map.put("expires_in", token.getExpiresIn()+"");
			
			
			OAuth2Authentication oAuth2Auth = tokenStore.readAuthentication(token);
			Authentication authentication = oAuth2Auth.getUserAuthentication();

			map.put("client_id", oAuth2Auth.getOAuth2Request().getClientId());
			map.put("grant_type", oAuth2Auth.getOAuth2Request().getGrantType());
			
			if (authentication instanceof UsernamePasswordAuthenticationToken) {
				UsernamePasswordAuthenticationToken authenticationToken = (UsernamePasswordAuthenticationToken) authentication;
			
				if(authenticationToken.getPrincipal() instanceof LoginAppUser){
					LoginAppUser user = (LoginAppUser) authenticationToken.getPrincipal();
					map.put("user_id", user.getId()+"");
					map.put("user_name", user.getUsername()+"");
					map.put("user_head_imgurl", user.getAvatar()+"");
				}
				
				
			}else if (authentication instanceof PreAuthenticatedAuthenticationToken ){
				//刷新token方式
				PreAuthenticatedAuthenticationToken authenticationToken = (PreAuthenticatedAuthenticationToken) authentication;
				if(authenticationToken.getPrincipal() instanceof LoginAppUser){
					LoginAppUser user = (LoginAppUser) authenticationToken.getPrincipal();
					map.put("user_id", user.getId()+"");
					map.put("user_name", user.getUsername()+"");
					map.put("user_head_imgurl", user.getAvatar()+"");
				}

			}
			list.add(map);

		}

		return new PageResult(list);

	}

	public List<String> findKeysForPage(String patternKey, int pageNum, int pageSize) {

		Set<String> execute = redisTemplate.execute(new RedisCallback<Set<String>>() {

			@Override
			public Set<String> doInRedis(RedisConnection connection) throws DataAccessException {

				Set<String> binaryKeys = new HashSet<>();

				Cursor<byte[]> cursor = connection
						.scan(new ScanOptions.ScanOptionsBuilder().match(patternKey).count(1000).build());
				int tmpIndex = 0;
				int startIndex = (pageNum - 1) * pageSize;
				int end = pageNum * pageSize;
				while (cursor.hasNext()) {
					if (tmpIndex >= startIndex && tmpIndex < end) {
						binaryKeys.add(new String(cursor.next()));
						tmpIndex++;
						continue;
					}

					// 获取到满足条件的数据后,就可以退出了
					if (tmpIndex >= end) {
						break;
					}

					tmpIndex++;
					cursor.next();
				}
				connection.close();
				return binaryKeys;
			}
		});

		List<String> result = new ArrayList<String>(pageSize);
		result.addAll(execute);
		return result;
	}

}
