package org.surge.oauth2.config;

import org.surge.common.props.PermitUrlProperties;
import org.surge.oauth2.handler.OauthLogoutHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

/**
 * spring security配置
 *
 * <p>在WebSecurityConfigurerAdapter不拦截oauth要开放的资源
 *
 * @author qipengpai
 * @date 2019/6/26 22:37
 * @since 0.0.1
 */
@Configuration
@EnableConfigurationProperties(PermitUrlProperties.class)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * 认证成功处理器
	 */
	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;

	/**
	 * 认证失败处理器
	 */
	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;

	@Autowired(required = false)
	private AuthenticationEntryPoint authenticationEntryPoint;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private OauthLogoutHandler oauthLogoutHandler;
	@Autowired
	private PermitUrlProperties permitUrlProperties ;

	/**
	 * Spring Security 忽略校验资源(17)
	 * @author qipengpai
	 * @date 2019/6/26 22:37
	 * @param web WebSecurity
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources", "/configuration/security",
				"/swagger-ui.html", "/webjars/**", "/doc.html", "/login.html");
		web.ignoring().antMatchers("/js/**");
		web.ignoring().antMatchers("/css/**");
		web.ignoring().antMatchers("/health");
		web.ignoring().antMatchers("/oauth/user/token");
		web.ignoring().antMatchers("/oauth/client/token");
		web.ignoring().antMatchers(permitUrlProperties.getIgnored());
		
	}

	/**
	 * 认证管理(7)
	 * @author qipengpai
	 * @date 2019/6/4 15:55
	 * @return 认证管理对象
	 * @throws Exception  认证异常信息
	 **/
	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	/**
	 * Spring Security中HttpSecurity配置程序允许的受保护资源的其他定制（16）
	 *
	 * <p>注意:如果您的授权服务器也是一个资源服务器，那么还有另一个安全过滤器链，
	 *   它的优先级较低，控制了API资源。 对于那些需要通过访问令牌来保护的请求，您需要它们的路径不能与主用户所面对的过滤器链中的那些相匹配，
	 * 	 所以一定要包含一个请求matcher，它只挑选出下面的WebSecurityConfigurer中的非Api资源。
	 * @author qipengpai
	 * @date 2019/6/26 22:45
	 * @param http HttpSecurity
	 **/
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		http.authorizeRequests()
				.anyRequest().authenticated();
		http.formLogin().loginPage("/login").loginProcessingUrl("/user/login")
				.successHandler(authenticationSuccessHandler).failureHandler(authenticationFailureHandler);

		// 基于密码 等模式可以无session,不支持授权码模式
		if (authenticationEntryPoint != null) {
			http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint);
			http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		} else {
			// 授权码模式单独处理，需要session的支持，此模式可以支持所有oauth2的认证
			http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
		}

		http.logout().logoutSuccessUrl("/login")
				.logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler())
				.addLogoutHandler(oauthLogoutHandler).clearAuthentication(true);

		// 解决不允许显示在iframe的问题
		http.headers().frameOptions().disable();
		http.headers().cacheControl();

	}


	/**
	 * 全局用户信息(6)
	 * @author qipengpai
	 * @date 2019/6/4 15:57
	 * @param auth 认证管理
	 * @throws Exception 用户认证异常信息
	 **/
	@Autowired
	public void globalUserDetails(AuthenticationManagerBuilder auth) throws Exception {
		// 实现 userDetailsService 与 passwordEncoder
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}

}
