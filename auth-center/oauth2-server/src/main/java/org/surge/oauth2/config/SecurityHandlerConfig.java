package org.surge.oauth2.config;

import org.surge.common.result.Result;
import org.surge.oauth2.handler.OauthLogoutHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.*;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 安全处理器配置
 * @author qipengpai
 * @date 2019/6/4 16:43
 * @since 0.0.1
 */
@Component
@Configuration
public class SecurityHandlerConfig {

	/**
	 * springmvc启动时自动装配json处理类
	 */
	@Resource
	private ObjectMapper objectMapper;



	/**
	 * 认证成功处理器，返回Token 装配此bean不支持授权码模式(1)
	 * @author qipengpai
	 * @date 2019/6/4 16:43
	 * @return org.springframework.security.web.authentication.AuthenticationSuccessHandler
	 */
	@Bean
	public AuthenticationSuccessHandler loginSuccessHandler() {
		return new SavedRequestAwareAuthenticationSuccessHandler() {

			private RequestCache requestCache = new HttpSessionRequestCache();

			@Override
			public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
					Authentication authentication) throws IOException, ServletException {

				super.onAuthenticationSuccess(request, response, authentication);
				return;

			}
		};
	}

	/**
	 * 认证失败处理器(2)
	 * @author qipengpai
	 * @date 2019/6/4 16:44
	 * @return org.springframework.security.web.authentication.AuthenticationFailureHandler
	 */
	@Bean
	public AuthenticationFailureHandler loginFailureHandler() {

		return (request,response,exception)->{
			String msg = null;
			if (exception instanceof BadCredentialsException) {
				msg = "密码错误";
			} else {
				msg = exception.getMessage();
			}
			response.setStatus(HttpStatus.UNAUTHORIZED.value());

			Result rsp = Result.failed(HttpStatus.UNAUTHORIZED.value(),msg);

			response.setContentType("application/json;charset=UTF-8");
			response.getWriter().write(objectMapper.writeValueAsString(rsp));
			response.getWriter().flush();
			response.getWriter().close();
		};

	}


	/**
	 * 配置异常翻译bean(8)
	 * @author qipengpai
	 * @date 2019/6/10 10:42
	 * @return org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator
	 **/
	@Bean
	public WebResponseExceptionTranslator webResponseExceptionTranslator() {
		return new DefaultWebResponseExceptionTranslator() {

			public static final String BAD_MSG = "Bad credentials";

			@Override
			public ResponseEntity<OAuth2Exception> translate(Exception e) throws Exception {
				OAuth2Exception oAuth2Exception;
				if (e.getMessage() != null && e.getMessage().equals(BAD_MSG)) {
					oAuth2Exception = new InvalidGrantException("密码错误", e);
				} else if (e instanceof InternalAuthenticationServiceException) {
					//用户名不存在
					oAuth2Exception = new InvalidGrantException(e.getMessage(), e);
				} else if (e instanceof RedirectMismatchException) {
					oAuth2Exception = new InvalidGrantException(e.getMessage(), e);
				} else if (e instanceof InvalidScopeException) {
					oAuth2Exception = new InvalidGrantException(e.getMessage(), e);
				} else {
					oAuth2Exception = new UnsupportedResponseTypeException("服务内部错误", e);
				}

				ResponseEntity<OAuth2Exception> response = super.translate(oAuth2Exception);
				ResponseEntity.status(oAuth2Exception.getHttpErrorCode());
				response.getBody().addAdditionalInformation("respCode", oAuth2Exception.getHttpErrorCode() + "");
				response.getBody().addAdditionalInformation("respMsg", oAuth2Exception.getMessage());
				return response;
			}

		};
	}

	/**
	 * 退出登录控制器(4)
	 * @author qipengpai
	 * @date 2019/6/25 15:48
	 * @return com.bccv.nmg.server.handler.OauthLogoutHandler
	 **/
	@Bean
	public OauthLogoutHandler oauthLogoutHandler() {
		return new OauthLogoutHandler();
	}
	 

}
