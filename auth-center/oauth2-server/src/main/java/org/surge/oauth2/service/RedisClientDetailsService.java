package org.surge.oauth2.service;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.common.exceptions.InvalidClientException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.security.oauth2.provider.client.BaseClientDetails;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import java.util.List;


/**
 * redis存储
 *
 * <p>将oauth_client_details表数据缓存到redis，这里做个缓存优化
 * 模块中如有有对oauth_client_details的crud， 注意同步redis的数据
 * 注意对oauth_client_details清楚redis db部分数据的清空
 *
 * @author qipengpai
 * @date 2019/6/10 10:49
 * @since 0.0.1
 */
@Slf4j
@Component
public class RedisClientDetailsService extends JdbcClientDetailsService {
	
    /**
     * 缓存client的redis key，这里是hash结构存储
     */
    private static final String CACHE_CLIENT_KEY = "oauth_client_details";

    @Autowired
    private RedisTemplate<String,Object> redisTemplate ;
    
    /*public RedisTemplate<String, Object> getRedisTemplate() {
		return redisTemplate;
	}*/

	public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	public RedisClientDetailsService(DataSource dataSource) {
        super(dataSource);
    }



    /**
     * 根据客户端id查询客户端
     * @author qipengpai
     * @date 2019/6/10 10:49
     * @param clientId 客户端ID
     * @return org.springframework.security.oauth2.provider.ClientDetails
     **/
    @Override
    public ClientDetails loadClientByClientId(String clientId) throws InvalidClientException {
        ClientDetails clientDetails = null;

        // 先从redis获取
        String value = (String) redisTemplate.boundHashOps(CACHE_CLIENT_KEY).get(clientId);
        if (StringUtils.isBlank(value)) {
            clientDetails = cacheAndGetClient(clientId);
        } else {
            clientDetails = JSONObject.parseObject(value, BaseClientDetails.class);
        }

        return clientDetails;
    }

    /**
     * 缓存client并返回client
     * @author qipengpai
     * @date 2019/6/10 10:47
     * @param clientId 客户端ID
     * @return org.springframework.security.oauth2.provider.ClientDetails
     **/
    private ClientDetails cacheAndGetClient(String clientId) {
        // 从数据库读取
        ClientDetails clientDetails = null ;
		try {
			clientDetails = super.loadClientByClientId(clientId);
			if (clientDetails != null) {            
				// 写入redis缓存
				redisTemplate.boundHashOps(CACHE_CLIENT_KEY).put(clientId, JSONObject.toJSONString(clientDetails));
			    log.info("缓存clientId:{},{}", clientId, clientDetails);
			}
		}catch (NoSuchClientException e){
            log.info("clientId:{},{}", clientId, clientId );
		}catch (InvalidClientException e) {
			//  Auto-generated catch block
			e.printStackTrace();
		}

        return clientDetails;
    }

    /**
     * 修改指定客户端
     * @author qipengpai
     * @date 2019/6/26 18:38
     * @param clientDetails 客户端详情
     **/
    @Override
    public void updateClientDetails(ClientDetails clientDetails) throws NoSuchClientException {
        super.updateClientDetails(clientDetails);
        cacheAndGetClient(clientDetails.getClientId());
    }

    /**
     * 修改指定客户端
     * @author qipengpai
     * @date 2019/6/26 18:39
     * @param clientId 客户端ID
     * @param secret 安全
     **/
    @Override
    public void updateClientSecret(String clientId, String secret) throws NoSuchClientException {
        super.updateClientSecret(clientId, secret);
        cacheAndGetClient(clientId);
    }

    /**
     * 移除指定客户端
     * @author qipengpai
     * @date 2019/6/25 15:46
     * @param clientId 客户端id
     **/
    @Override
    public void removeClientDetails(String clientId) throws NoSuchClientException {
        super.removeClientDetails(clientId);
        removeRedisCache(clientId);
    }

    /**
     * 删除此客户端redis缓存
     * @author qipengpai
     * @date 2019/6/10 10:46
     * @param clientId 客户端id
     **/
    private void removeRedisCache(String clientId) {
    	redisTemplate.boundHashOps(CACHE_CLIENT_KEY).delete(clientId);
    }

    /**
     * 将oauth_client_details全表刷入redis
     * @author qipengpai
     * @date 2019/6/10 10:46
     **/
    public void loadAllClientToCache() {
        if (redisTemplate.hasKey(CACHE_CLIENT_KEY)) {
            log.error("oauth_client_details已存在于redis");
            return;
        }
        log.info("将oauth_client_details全表刷入redis");

        List<ClientDetails> list = super.listClientDetails();
        if (CollectionUtils.isEmpty(list)) {
            log.error("oauth_client_details表数据为空，请检查");
            return;
        }

        list.parallelStream().forEach(client -> {
        	redisTemplate.boundHashOps(CACHE_CLIENT_KEY).put(client.getClientId(), JSONObject.toJSONString(client));
        });
    }
}
