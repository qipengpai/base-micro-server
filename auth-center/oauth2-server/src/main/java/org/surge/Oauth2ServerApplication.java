package org.surge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 微服务认证中心
 * @author qipengpai
 * @since 0.0.1
 * @date 2019/6/2 17:06
 **/
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class Oauth2ServerApplication {

    /**
     * --spring.profiles.active=dev1
     * -Xms256m -Xmx256m
     * 本地启动采用此方法加载profiles文件
     * <code>ConfigurableApplicationContext context = new SpringApplicationBuilder(Oauth2ServerApplication.class).
     * profiles("dev").run(args)</code>
     * <code>SpringApplication.run(Oauth2ServerApplication.class, args)</code>
     * 服务器采用此方法 java -jar --spring.profiles.active=dev;
     */
    public static void main(String[] args) {
        SpringApplication.run(Oauth2ServerApplication.class, args);
    }

}
