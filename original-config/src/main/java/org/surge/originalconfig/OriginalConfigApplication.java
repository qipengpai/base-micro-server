package org.surge.originalconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Config配置中心微服务
 * @author qipengpai
 * @date 2019/6/12 11:52
 * @since 0.0.1
 */
@EnableConfigServer
@EnableEurekaClient
@SpringBootApplication
public class OriginalConfigApplication {

    /**
     * --spring.profiles.active=dev1
     * -Xms256m -Xmx256m
     * 本地启动采用此方法加载profiles文件
     * <code>ConfigurableApplicationContext context = new SpringApplicationBuilder(OriginalConfigApplication.class).
     * profiles("slave1").run(args)</code>
     * <code>SpringApplication.run(OriginalConfigApplication.class, args)</code>
     * 服务器采用此方法 java -jar --spring.profiles.active=slave3;
     */
    public static void main(String[] args) {
        SpringApplication.run(OriginalConfigApplication.class, args);
    }

}

