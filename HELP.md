#IDEA 配置完善
##启动报错Command line is too long
```
Error running 'Oauth2ServerApplication': Command line is too long. 
Shorten command line for Oauth2ServerApplication or also for Spring Boot default configuration
```
在该项目文件夹.idea/workspace.xml中找到
```
<component name="PropertiesComponent">
  ...
</component>
```
然后在其中添加:
```
<property name="dynamic.classpath" value="true" />
```
##配置自动启动Run Dashboard
RunDashboard 下加入
```
<option name="configurationTypes">
      <set>
        <option value="SpringBootApplicationConfigurationType" />
      </set>
</option>
```

