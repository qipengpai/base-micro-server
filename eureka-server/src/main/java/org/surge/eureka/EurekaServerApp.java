package org.surge.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * eureka 服务器
 * @author qipengpai
 * @date  2018-12-18 22:50:29
 * @since 0.0.1
 */
@EnableEurekaServer
@SpringCloudApplication
public class EurekaServerApp{

	/**
	 * --spring.profiles.active=dev1
	 * -Xms128m -Xmx128m
	 * 本地启动采用此方法加载profiles文件
	 * <code>ConfigurableApplicationContext context = new SpringApplicationBuilder(EurekaServerApp.class).
	 * profiles("slave1").run(args)</code>
	 * <code>SpringApplication.run(EurekaServerApp.class, args)</code>
	 * 服务器采用此方法 java -jar --spring.profiles.active=slave3;
	 */
	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApp.class, args);
	}

}