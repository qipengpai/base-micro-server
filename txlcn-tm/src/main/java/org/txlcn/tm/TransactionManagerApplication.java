package org.txlcn.tm;

import com.codingapi.txlcn.tm.config.EnableTransactionManagerServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * 事务协调者
 * @author qipengpai
 * @date 2019/7/5 16:49
 * @since 0.0.1
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableTransactionManagerServer
public class TransactionManagerApplication {

    /**
     * --spring.profiles.active=dev1
     * -Xms256m -Xmx256m
     * 本地启动采用此方法加载profiles文件
     * <code>ConfigurableApplicationContext context = new SpringApplicationBuilder(TransactionManagerApplication.class).
     * profiles("dev").run(args)</code>
     * <code>SpringApplication.run(TransactionManagerApplication.class, args)</code>
     * 服务器采用此方法 java -jar --spring.profiles.active=dev;
     */
    public static void main(String[] args) {
        SpringApplication.run(TransactionManagerApplication.class, args);
    }
}
