package org.surge.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * zuul网关
 * @see EnableZuulProxy
 * @author qipengpai
 * @date  2018-12-18 22:50:29
 * @since 0.0.1
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class ZuulApplication {

    /**
     * --spring.profiles.active=dev1
     * -Xms256m -Xmx256m
     * 本地启动采用此方法加载profiles文件
     * <code>ConfigurableApplicationContext context = new SpringApplicationBuilder(ZuulApplication.class).
     * profiles("dev").run(args)</code>
     * <code>SpringApplication.run(ZuulApplication.class, args)</code>
     * 服务器采用此方法 java -jar --spring.profiles.active=slave3;
     */
    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class, args);
    }
}
