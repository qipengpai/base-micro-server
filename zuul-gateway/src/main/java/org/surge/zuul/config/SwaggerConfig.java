package org.surge.zuul.config;

import com.didispace.swagger.butler.EnableSwaggerButler;
import org.springframework.context.annotation.Configuration;

/**
 *
 * swagger 聚合文档配置
 *
 * <p>zuul routers 映射具体服务的/v2/api-docs swagger
 * Swagger Butler是一个基于Swagger与Zuul构建的API文档汇集工具。
 * 通过构建一个简单的Spring Boot应用，增加一些配置就能将现有整合了Swagger的Web应用的API文档都汇总到一起，方便查看与测试
 *
 * @author qipengpai
 * @date  2018-12-18 22:50:29
 * @since  0.0.1
 */
@Configuration
@EnableSwaggerButler
public class SwaggerConfig {

}
