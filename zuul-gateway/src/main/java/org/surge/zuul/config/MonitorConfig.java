package org.surge.zuul.config;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * 监视器Bean
 * @author qipengpai
 * @date  2018-12-18 22:50:29
 * @since 0.0.1
 */
@Configuration
public class MonitorConfig {

    /**
     * 需要注入HystrixMetricsStreamServlet（第三方的servlet），该servlet是hystrix的组件
     * @author qipengpai
     * @date 2019/6/20 14:36
     * @return org.springframework.boot.web.servlet.ServletRegistrationBean
     */
	@Bean
    public ServletRegistrationBean<HystrixMetricsStreamServlet> getServlet() {
        HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
        ServletRegistrationBean<HystrixMetricsStreamServlet> registrationBean = new ServletRegistrationBean<>(streamServlet);
        registrationBean.setLoadOnStartup(1);
        registrationBean.addUrlMappings("/hystrix.stream");
        registrationBean.setName("HystrixMetricsStreamServlet");
        return registrationBean;
    }      
}
