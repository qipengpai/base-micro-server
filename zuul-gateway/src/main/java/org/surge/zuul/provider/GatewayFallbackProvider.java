package org.surge.zuul.provider;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.netflix.hystrix.exception.HystrixTimeoutException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.route.FallbackProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.surge.common.result.CodeEnum;

import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * 自定义Zuul回退机制处理器。
 * <p>Provides fallback when a failure occurs on a route
 * 英文意思就是说提供一个回退机制当路由后面的服务发生故障时。
 * @author qipengpai
 * @date  2018-12-18 22:50:29
 * @since 0.0.1
 */
@Slf4j
@Component
public class GatewayFallbackProvider implements FallbackProvider {

	/**
	 * 全部微服务提供回退
	 *
	 * <p>返回值表示需要针对此微服务做回退处理（该名称一定要是注册进入 eureka 微服务中的那个 serviceId 名称）；
	 * 表明是为哪个微服务提供回退，*表示为所有微服务提供回退
	 *
	 * @author qipengpai
	 * @date  2018-12-18 22:52:29
	 * @return java.lang.String
	 */
	@Override
	public String getRoute() {
		return "*";
	}



	/**
	 * 熔断处理
	 *
	 * <p>网关向api服务请求是失败了，但是消费者客户端向网关发起的请求是OK的，
	 * 不应该把api的404,500等问题抛给客户端
	 * 网关和api服务集群对于客户端来说是黑盒子
	 *
	 * @param route 理由
	 * @param cause 异常
	 * @return ClientHttpResponse
	 */
	@Override
	public ClientHttpResponse fallbackResponse(String route, Throwable cause){

		if(cause instanceof HystrixTimeoutException){
			return response(HttpStatus.GATEWAY_TIMEOUT);
		}else{
			return this.fallbackResponse();
		}
	}

	public ClientHttpResponse fallbackResponse(){
		return this.response(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	private ClientHttpResponse response(final HttpStatus status){

		return new ClientHttpResponse() {
			@Override
			public HttpStatus getStatusCode() throws IOException {
				return status;
			}
			@Override
			public int getRawStatusCode() throws IOException{
				return status.value();
			}
			@Override
			public String getStatusText() throws IOException{
				return status.getReasonPhrase();
			}
			@Override
			public void close(){
			}
			 /**
              * 当 springms-provider-user 微服务出现宕机后，客户端再请求时候就会返回 fallback 等字样的字符串提示；
              * 但对于复杂一点的微服务，我们这里就得好好琢磨该怎么友好提示给用户了；
              * 如果请求用户服务失败，返回什么信息给消费者客户端
              * @return InputStream
              * @throws IOException IO操作异常
              */
			@Override
			public InputStream getBody() throws IOException {
				JSONObject r = new JSONObject();
				try {
					r.put("respCode", CodeEnum.SYS_ERROR.getRetCode());
					r.put("respMsg", CodeEnum.SYS_ERROR.getRetMsg());
				} catch (JSONException e) {
					log.error("系统错误，错误原因:{}" , e.getStackTrace()[0] );
				}
				return new ByteArrayInputStream(r.toString().getBytes(StandardCharsets.UTF_8));

			}
			@Override
			public HttpHeaders getHeaders(){

				// headers设定
				HttpHeaders headers = new HttpHeaders();
				MediaType mt = new MediaType("application", "json", StandardCharsets.UTF_8);
				headers.setContentType(mt);
				return headers;
			}

		};

	}

}