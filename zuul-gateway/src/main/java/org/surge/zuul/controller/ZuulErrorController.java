package org.surge.zuul.controller;

import com.netflix.zuul.context.RequestContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.surge.common.result.Result;


/**
 * (限流处理异常)
 * zuul自定义异常格式
 * @author qipengpai
 * @date  2018-12-18 22:50:29
 * @since 0.0.1
 */
@Slf4j
@Controller
public class ZuulErrorController implements ErrorController {

    private static final String ERROR_PATH = "/error";

    /**
     * 错误最终会到这里来
     * @author qipengpai
     * @date 2019/8/13 16:20
     * @return java.lang.Object
     */
    @RequestMapping(ERROR_PATH)
    @ResponseBody
    public Object error() {
        RequestContext ctx = RequestContext.getCurrentContext();
        Throwable throwable = ctx.getThrowable();
        return Result.failed(throwable.getMessage()) ;
    }

    

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}